#!/bin/sh

JBOSS_HOME=@@@as.dest.dir.full@@@
rm -rf $JBOSS_HOME/standalone/configuration
rm -rf $JBOSS_HOME/standalone/tmp
rm -rf $JBOSS_HOME/standalone/data
mkdir -p $JBOSS_HOME/standalone/configuration/ && cp @@@as.config.dir.source@@@/* $JBOSS_HOME/standalone/configuration/
sh $JBOSS_HOME/bin/standalone.sh --debug @@@as.debug.port@@@